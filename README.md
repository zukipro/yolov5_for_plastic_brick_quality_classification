## General

This code is associated with the dataset descriptor titled "Simplified Object Detection for Manufacturing: Introducing a Low-Resolution Dataset", published in the ing.grid Journal. The dataset can be accessed via the Zenodo repository at https://zenodo.org/records/10838894. 

This software is based on the ultralytics/yolov5 project and is distributed under the terms of the GNU Affero General Public License v3.0. All modifications made to the original code can be found within the provided source code, which is available for review and distribution in compliance with the aforementioned license.

## Citation 

This Software uses YOLOv5 by Ultralytics.

  cff-version: 1.2.0
  preferred-citation:
  - authors:
    family-names: Jocher
    given-names: Glenn
    orcid: "https://orcid.org/0000-0001-5950-6979"
  - title: "YOLOv5 by Ultralytics"
  - version: 7.0
  - doi: 10.5281/zenodo.3908559
  - date-released: 2020-5-29
  - license: AGPL-3.0
  - url: "https://github.com/ultralytics/yolov5"

This Software uses Roboflow.

cff-version: 1.2.0:
- title: Roboflow Inference Server
- type: software
authors:
  - given-names: Robfolow
    email: support@roboflow.com
- repository-code: 'https://github.com/roboflow/inference-server'
- url: 'https://roboflow.com'
abstract: >-
  An opinionated, easy-to-use inference server for use with
  computer vision models.
keywords:
  - computer vision
  - inference
license: Apache-2.0

## Training procedure

The datasets were captured by an customized .py file corresponding to our camera, annotated via Roboflow and saved in /dataset

The four object detection models were trained by /train.py, and saved in /runs/train

The trained models were validated with the same validation set and testing set using /val.py, and saved in /runs/val

