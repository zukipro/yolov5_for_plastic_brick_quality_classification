import cv2
import torch
from models.experimental import attempt_load
from utils.general import non_max_suppression

# Load YOLOv5 model (replace 'yolov5s.pt' with the path to your model weights)
model = attempt_load('runs/train/exp9/weights/best.pt')

# Set the camera resolution
camera_width = 1280  # Change this to your desired width
camera_height = 720  # Change this to your desired height

# Open the camera
cap = cv2.VideoCapture(0)  # Use the appropriate camera index (0 for the default camera)

while True:
    ret, frame = cap.read()
    if not ret:
        break
    
    # Resize the frame to your desired dimensions
    frame = cv2.resize(frame, (camera_width, camera_height))
    
    # Perform YOLOv5 inference on the resized frame
    results = model(frame)
    
    # Post-process the results (e.g., apply NMS, draw bounding boxes)
    # ...

    # Display the processed frame
    cv2.imshow('YOLOv5 Object Detection', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()